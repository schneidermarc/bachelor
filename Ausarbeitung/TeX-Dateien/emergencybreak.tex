% !TEX root = ../thesis.tex
% !TeX spellcheck = de_DE

\chapter{Die Notbremse}
\label{txt:emergencybreak}

In diesem Kapitel wird die Notbremse vorgestellt. Diese Komponente ist für den Sicherheitsaspekt des Systems verantwortlich und dient zur permanenten Überprüfung der Fahrumgebung. Um zu verdeutlichen, wie diese Sicherheitsmechanismen umgesetzt werden, müssen zunächst Fahrzeugeigenschaften näher erläutert werden. Außerdem müssen Fahrszenarien vorgestellt werden, um verständlich zu machen, was das Konzept hinter einzelnen Entwicklungsschritten ist.

Die Notbremse ist die Komponente des Systems, die durch Umgebungsüberwachung in Form von Abständen für die Sicherheit sorgt. Dabei erfolgt eine Umgebungserfassung durch diese Sensoren. Wie aus \cref{txt:concept} entnommen werden kann, läuft diese Komponente parallel zu allen anderen Komponenten und ist so zu jedem Zeitpunkt in der Lage, das Fahrzeug rechtzeitig vor einer Kollision zu stoppen und eine Notbremsung einzuleiten. Um verständlich zu machen, wie die Umsetzung der Notbremse funktioniert und was Probleme bei der Realisierung sind, müssen zunächst wichtige Fahrzeugeigenschaften erörtert werden. Der zentrale Punkt bei einer Notbremse ist grundsätzlich das rechtzeitige Abbremsen bevor es zu einem Zusammenstoß kommt, hierbei gilt es zu berücksichtigen, dass das Modellauto nicht über eine eigene Bremse verfügt. Das Auto bremst durch seinen eigenen Trägheitsmoment. Zusätzlich, da es sich hier um ein elektrobetriebenes Fahrzeug handelt, hat das Wegnehmen der Geschwindigkeit bzw. das Setzen der Geschwindigkeit auf Null eine indirekte elektrische Bremswirkung. Mit der elektrischen Bremswirkung wird das Umwandeln der Bewegungsenergie in andere Energieformen und Kräfte bezeichnet. Hierzu dient ein innerer Widerstand der Antriebsmaschine. Die Bremswirkung wird dabei alleine durch das Wegnehmen der Treibenergie hervorgerufen. Bei der Entwicklung konnte beobachtet werden, dass die Geschwindigkeit bei einem Einparkvorgang so gering ist, dass diese Bremswirkung in Kombination mit dem Trägheitsmoment ausreicht um das Fahrzeug innerhalb weniger Zentimeter zu stoppen, was bei diesem Einparkvorgang als ausreichend angesehen wird.

Um detaillierter zu verstehen, was bei der Bremsdauer, dem Bremsweg und der weiteren Entwicklung berücksichtigt werden musste wird grundsätzlich zwischen zwei Notfallszenarien unterschieden. Das erste Notfallszenario ist, dass das Fahrzeug während des Einparkvorgangs von der vorgesehenen Fahrbahn abkommt und so eine unvorhergesehene Position im Straßenverkehr hat. Das alleinige Verlassen der Spur kann hierbei nicht ohne weiteres erkannt werden, es würde lediglich bei einem drohenden Zusammenstoß ein Bremsvorgang eingeleitet werden. Das zweite Szenario hingegen ist die bekannte Kollisionsvermeidung bei plötzlich auftauchenden Hindernissen. Was in den Versuchen durch das plötzliche Einschieben von Objekten getestet wurde, ist im reellen Straßenverkehr ein plötzlich vom Seitenrand ausparkendes oder herausfahrendes Fahrzeug, Fußgänger, die die Straße überqueren oder auf die Fahrbahn fallende Gegenstände. Beide Szenarien sollen bei der Notbremse durch das gleiche Prinzip gelöst werden, die Sensoren erfassen die nähere Umgebung des Fahrzeugs und leiten bei Unterschreitung eines Sicherheitsabstandes den Bremsvorgang ein.

Die für die Notbremse verwendeten Sensoren sind die in \cref{txt:sensors} beschriebenen \gls{gls:lidar}- und Ultraschallsensoren. Dabei überwacht der \gls{gls:lidar}-Sensor den vor dem Auto liegenden Teil und die Ultraschallsensoren die seitliche und hintere Umgebung. Bei der richtigen Wahl der einzelnen Sicherheitsabstände für jede Seite bzw. für jeden Sensor sind die oben genannten Fahrszenarien zu berücksichtigen.

Im ersten Szenario, dem plötzlichen Auftauchen eines Objekts, muss nur die vordere Umgebung berücksichtigt werden, denn die Fahrtrichtung schließt eine Kollision nach hinten aus. Da für diesen Teil der \gls{gls:lidar}-Sensor verantwortlich ist, wurde hierbei ein Sicherheitsabstand von 10\textendash 15\,cm veranschlagt. Diese Distanz reicht in jedem Fall um einen rechtzeitigen Stillstand des Fahrzeugs zu sichern. Nur bei Objekten die in einer Distanz unter 5\,cm auftauchen ist eine Kollision nicht mehr zu verhindern. Dieses Szenario ist gleichzeitig ein Bestandteil des nächsten Szenarios.

Das zweiten Szenario baut auf der Notbremse, die aus dem ersten Szenario hervorging, auf, da diese Fälle hier ebenfalls berücksichtigt werden müssen. Die aus dem ersten Szenario hervorgegangenen Überlegungen werden um die Sicherheitsabstände erweitert, die bei dem Einparkvorgang relevant sind. Die seitlichen Ultraschallsensoren zeigten sich als vernachlässigbar, da in diesen Fällen durch die versetzte Lage des Fahrzeugs, Hindernisse meist im toten Winkel der Sensoren lagen. Der auf der rechten Seite befindliche Sensor dient bereits zur Messung des Abstandes zum Fahrbahnrand. Außerdem zeigte sich währen der folgenden Evaluation, dass ein Notbremsvorgang bei einem auf der Seite auftauchenden Objekt sogar kontraproduktiv sein kein, da durch die Weiterfahrt eine natürliche Ausweichbewegung statt findet. Die restlichen drei Sensoren am Heck sind hingegen relevant um bei einer Fehlkalkulation der Parklückengröße oder einem fehlerhaften Einparkvorgang eine Kollision mit dem hinteren Objekt der Parklücke zu vermeiden. Da dies im Regelfall oft andere Fahrzeuge oder schwere Gegenstände sind, kann hierdurch ein Blechschaden vermieden werden. In \cref{txt:parkingprocess} wurde ein Puffer von 5 \textendash 10\,cm für die Parklücke berechnet. Dieser Puffer ist größer als der ermittelte Sicherheitsabstand für die Notbremse. Dies garantiert einen rechtzeitigen Stillstand des Fahrzeugs, ohne dabei möglicherweise den Einparkvorgang ohne notwendiges Eingreifen zu früh zu stoppen und den Einparkvorgang vor der Beendigung zu unterbrechen.

Abschließend kann festgehalten werden, dass das Notbremssystem durch die permanente Überwachung unabdingbar zur Gewährleistung der Sicherheit ist. Das komplette Umfeld des Fahrzeugs wird hierbei berücksichtigt und eine Einhaltung der Sicherheitsabstände gewährleistet. Hierbei muss ein Mittelmaß gefunden werden, zwischen dem rechtzeitigen Eingreifen vor einer Kollision und dem zu frühen Eingreifen, sodass unerwünscht Fahrmanöver unterbrochen werden. Wie solche Werte ermittelt wurden und die Notbremse getestet wird, wird in der folgenden Evaluation näher erörtert. Dazu werden die oben erwähnten Versuche genauer dargestellt und erklärt.

\section{Evaluation der Notbremse}
\label{sec_evaluationnotbremse}

Das Testen der Notbremse erwies sich als kompliziert, da hier unterschiedliche Szenarien im Straßenverkehr berücksichtigt werden mussten. Als Grundlage dienen die in \cref{txt:emergencybreak} beschrieben Verkehrsszenarien. Um den Fall eines plötzlich auftauchenden Hindernisses zu imitieren wurden unterschiedliche Objekte in den Fahrtweg des Fahrzeugs gestoßen; exemplarisch wurden Kartons oder andere Modellautos verwendet. Im zweiten Testszenario, in dem während des Einparkvorgangs die Parklücke verkleinert wurde, wurde das Objekt, was die Parklücke nach hinten begrenzt -zumeist Kartons oder andere Modellautos- nach vorne gezogen um so die Lücke kleiner zu machen als zunächst vom System angenommen.

\begin{figure}[h]
	\begin{subfigure}{0.9\textwidth}
		\centering
		\includegraphics[width=1\linewidth]{Literatur/Pictures/emergencybreak1scale.png}
		\caption{Seitliche Perspektive auf Szenario 1 der Notbremse}
		\label{fig:seitliche Perspektive} 
	\end{subfigure}
	
	\vspace*{0.5cm}
	\begin{subfigure}{0.9\textwidth}
		\centering
		\includegraphics[width=1\linewidth]{Literatur/Pictures/emergencybreak2scale.png}
		\caption{Vogelperspektive auf Szenario 1 der Notbremse}
		\label{fig:Vogelperspektive}
	\end{subfigure}
	
	\label{pic_notbremse}
	\caption[Versuchsaufbau für die Notbremse]{Versuchsaufbau für die Notbremse}
\end{figure}

Zur Evaluation der Notbremse wurden beide Szenarien in verschiedenen Abläufen getestet. Hierbei tauchte im ersten Szenario das Hindernis in immer kleiner werdenden Distanzen auf (siehe \cref{fig:seitliche Perspektive} und \cref{fig:Vogelperspektive}) und im zweiten Szenario wurde die ursprüngliche Größe der Parklücke von einem Meter kontinuierlich verkleinert (zum Vergleich siehe \cref{pic_einparkvorgang}).

In den folgenden Tabellen,  \cref{table_experimentsEmergencybreakS1} und \cref{table_experimentsEmergencybreakS2}, sind zur Evaluation der Ergebnisse die einzelnen Versuche mit den entsprechenden Beobachtungen festgehalten.

\begin{center}
	\begin{longtable}{ r l}
		
		\textbf{Distanz des auftauchenden Hindernisses} & \textbf{Beobachtung S1}\\
		\hline
		30\,cm & keine Kollision\\
		25\,cm & keine Kollision\\
		20\,cm & keine Kollision\\
		15\,cm & keine Kollision\\
		10\,cm & Kollision\\
		\caption{Experimente zur Notbremse in Szenario 1}\\
		\label{table_experimentsEmergencybreakS1}\\
		
	\end{longtable}
\end{center}

\begin{center}
	\begin{longtable}{ r l}
		
		\textbf{Veränderte Größe der Parklücke} & \textbf{Beobachtung S2}\\
		\hline
		100\,cm & keine Kollision\\
		95\,cm & keine Kollision\\
		90\,cm & keine Kollision\\
		85\,cm & Kollision\\
		
		\caption{Experimente zur Notbremse in Szenario 2}\\
		\label{table_experimentsEmergencybreakS2}\\
	\end{longtable}
\end{center}


Beide Szenarien zeigten ähnliche Ergebnisse. Bei Szenarien in denen Hindernisse in einer Distanz von unter 10\,cm oder in einem toten Winkel der Sensoren auftauchten, war ein Zusammenstoß nicht mehr zu verhindern. Außerdem zeigten die unterschiedlichen Beschaffenheiten mancher Oberflächen, dass diese das Ultraschallsignal oder Lichtsignal nicht ausreichend reflektierten und so nicht erkannt werden konnten. In seltenen Versuchen, in denen andere Modellautos einen ungewöhnlichen Winkel zum Fahrzeug hatten und so ihre schrägen Oberflächen das Signal nicht in Richtung des Empfängers reflektierten, konnten auch diese nicht eindeutig erkannt werden. Versuche in beiden Szenarien führten zu Unfällen, dennoch konnte abgesehen von diesen Fällen das Notbremssystem eine sichere und zuverlässige Funktion aufweisen.

Auch im Bereich der Notbremse wurden anschließend die gleichen Versuche wiederholt um die Ergebnisse zu verifizieren. So wurden in Szenario 1 mehrere Experimente durchgeführt, bei denen das Hindernis im Bereich von 10\,cm auftauchte und in Szenario 2 wurden dementsprechend auch weitere Versuche durchgeführt, bei denen die veränderte Parklückengröße bei etwa 85\,cm lag. Bei beiden Szenarien wurden kleinere Schwankungen um wenige Zentimeter eingeführt um so ein Abtasten dieser Schwellenwerte zu testen. So konnte die in den beiden Tabellen festgehaltenen Beobachtungen gefestigt und bestätigt werden.