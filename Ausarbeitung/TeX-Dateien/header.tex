% !TEX root = thesis.tex

% Load used-defined config
\input{config}

% Silence remreset -- https://tex.stackexchange.com/q/438543
\RequirePackage{silence}
\WarningFilter{remreset}{The remreset package}

\documentclass[
    %draft, % enable for quick rendering
    paper=a4, twoside, open=right,
    fontsize=12pt, parskip=half-,
    BCOR=20mm, DIV=12, headsepline,
    abstract=true, headings=chapterprefix, toc=bib, toc=listof]
    {scrreprt}

%%%%%%%%%%%%
% Packages %
%%%%%%%%%%%%

% Should go first:

% KOMA fixes + page layout
\usepackage{scrhack}
\usepackage[automark]{scrlayer-scrpage}

% Font control
\usepackage{fontspec}

\usepackage{algorithmic}

% Math support
\usepackage{amsmath}
\usepackage{unicode-math}

% Font selection (needs to go before polyglossia)
\usepackage{libertinus}

% Language control
\usepackage{polyglossia}
\setdefaultlanguage[variant=german, latesthyphen=true]{german}
\setotherlanguage[variant=usmax]{english}

% Date formats
\usepackage[useregional]{datetime2}
\DTMsavedate{submissiondate}{\submissionisodate}

% Write Metadata
\begingroup
\DTMsetdatestyle{iso}
\edef\wbgroup{\iftrue\string{\else}\fi}
\edef\wegroup{\iffalse{\else\string}\fi}
\newwrite\metadatafile
\immediate\openout\metadatafile=\jobname.xmpdata
\immediate\write\metadatafile{\unexpanded{\Author}\wbgroup\authorgivenname\space\authorsurname\wegroup}
\immediate\write\metadatafile{\unexpanded{\Title}\wbgroup\thesistitle\wegroup}
\immediate\write\metadatafile{\unexpanded{\Date}\wbgroup\DTMtoday\wegroup}
\immediate\write\metadatafile{\unexpanded{\Keywords}\wbgroup\thesistype arbeit\wegroup}
\immediate\write\metadatafile{\unexpanded{\PublicationType}\wbgroup book\wegroup}
\immediate\closeout\metadatafile
\endgroup

% PDF/A setup + Hyperlinks + Color
\PassOptionsToPackage{obeyspaces}{url}
\PassOptionsToPackage{svgnames}{xcolor}
\usepackage[a-3a]{pdfx}
\hypersetup{breaklinks, hidelinks, pageanchor}
\usepackage{hyperref}

% Algorithms
\usepackage[algochapter, linesnumbered, vlined]{algorithm2e}

% Author and title reuse
\usepackage{authoraftertitle}

% Bibliography
\usepackage[backref, style=alphabetic]{biblatex}

% Print-quality tables
\usepackage{booktabs}

% Automatic reference types
\usepackage[capitalise, noabbrev]{cleveref}

% Watermarks for draft versions
%\usepackage{draftwatermark}
%\SetWatermarkAngle{57.5}
%\SetWatermarkLightness{.95}
%\SetWatermarkText{ENTWURF \(\alpha\).1}

% Custom enumerations
\usepackage{enumitem}

% Layout control for the cover
\usepackage[pass]{geometry}

% Image inclusion
\usepackage{graphicx}
\usepackage{float}
\usepackage{placeins}

% Glossary
\usepackage[abbreviations, record=only, toc]{glossaries-extra}

% Page-breaking tables with auto-sized columns
\usepackage{ltxtable}

% Enable microtypography support
\usepackage[final]{microtype}

% Listings with syntax highlighting; requires --shell-escape
\usepackage[newfloat]{minted}

% Fine spacing control for math
\usepackage{mleftright}

% PDF inclusion
\usepackage{pdfpages}

% Relative font sizes
\usepackage{relsize}

% Subfigures
\usepackage{subcaption}

%Units
\usepackage{siunitx}

% Tagged PDF structure
\usepackage{tagpdf}
\tagpdfsetup{activate-all}

% Definitions, theorems etc
\usepackage{thmtools}

%Tabulars
\usepackage{tabularx}
\usepackage{longtable}

% Drawings in LaTeX
\usepackage{tikz}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{babel}
\usetikzlibrary{backgrounds}
\usetikzlibrary{bending}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.markings}
\usetikzlibrary{decorations.pathreplacing}
\usetikzlibrary{external} % requires --shell-escape
\usetikzlibrary{fadings}
\usetikzlibrary{matrix}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes}
\tikzsetexternalprefix{tikz-externals}
% \tikzexternalize Render TikZ externally, fails for some references

% URL typesetting
\usepackage{url}

% Conditionals
\usepackage{xifthen}

% Needs to go last:

% Language-sensitive quotation marks
\usepackage{csquotes}

% Break URLs at / and -
\def\UrlBreaks{\do\/\do-}

%%%%%%%%%%%%%%%%%%%%
% Style and layout %
%%%%%%%%%%%%%%%%%%%%

% Only list sections in the TOC
\setcounter{tocdepth}{2}

% Header and footer
\addtokomafont{pageheadfoot}{\upshape\sffamily\bfseries}
\addtokomafont{pagenumber}{\sffamily\bfseries}

\renewcommand*{\chaptermarkformat}{}
\renewcommand*{\sectionmarkformat}{}

\clearscrheadfoot
\ohead{\headmark}
\ihead{\pagemark}

\automark[chapter]{chapter}
\automark*[section]{}

\pagestyle{scrheadings}

% Underlined chapter titles
\newcommand\titlerule[1][.5pt]{\rule{\textwidth}{#1}}

\RedeclareSectionCommand[innerskip=0pt]{chapter}

\renewcommand\chapterlineswithprefixformat[3]{%
    \setlength{\parskip}{0pt}%
    #2\nobreak%
    \ifstr{#2}{}{}{\kern-\dp\strutbox}%
    \titlerule\par\nobreak%
    #3%
    \par\nobreak\titlerule%
}

% Neat + for et al
\renewcommand*{\labelalphaothers}{\raisebox{.3ex}{\relsize{-3}{\bfseries +}}}
\renewcommand*{\sortalphaothers}{+}

% Remove algorithm captions, see examples
\renewcommand{\AlCapSty}{}

% Use minted's line numbers for algorithm2e
\let\vrbstyle\theFancyVerbLine
\patchcmd{\vrbstyle}{\arabic{FancyVerbLine}}{}{}{}
\SetNlSty{vrbstyle}{}{}

% Pastel colored listings
\usemintedstyle{friendly}

% Glossary styling
\setabbreviationstyle{short}
\renewcommand{\glspostdescription}{\quad}

% German strings
\addto\captionsgerman{%
    \renewcommand{\listlistingname}{Listingverzeichnis}%
}

%Roman numbers
\newcommand{\uproman}[1]{\uppercase\expandafter{\romannumeral#1}}

%%%%%%%%%%%
% Content %
%%%%%%%%%%%

% Load external resources
\addbibresource{Literatur/LiteraturBib.bib}
\GlsXtrLoadResources[src={Literatur/glossary.bib}]

% Internal metadata setup
\author{\authorgivenname~\authorsurname}
\title{\thesistitle}
\date{\DTMusedate{submissiondate}}