% !TEX root = ../thesis.tex
% !TeX spellcheck = de_DE

\chapter{Die Sensorik}
\label{txt:sensors}

In diesem Kapitel werden der Radsensor, die Ultraschallsensoren, die inertiale Messeinheit sowie der \gls{gls:lidar}-Sensor thematisiert und ihre technischen Details genauer vorgestellt. Dabei wird erörtert, wie aus diesen Sensoren die relevanten Daten entnommen werden, die für die späteren Komponenten wichtig sind. Wie in \cref{txt:concept} bereits erläutert, ist das Messen von zurückgelegten Distanzen sowie Entfernungen zu Hindernissen für die Komponente der Parklückenfindung relevant. Es wird genauer erklärt, wie aus dem \gls{gls:lidar}-Sensor die Daten zur Winkelbestimmung gewonnen werden können, die für die Realisierung des Einparkvorgangs entscheidend sind. Außerdem wird in diesem Zusammenhang verdeutlicht, warum die vorhandene inertiale Messeinheit (engl. Inertial Measurement Unit \gls{gls:imu}) sich nicht für die Positionsbestimmung sowie der Winkeldaten nutzen lies und diese Daten daher ungeeignet waren. Weiter wird darauf eingegangen, wie die Daten des Ultraschallsensors, der einen wesentlichen Bestandteil der Notbremse und der Parklückenvermessung darstellt, aufbereitet werden können, um sie so sicherer zu nutzen.

\FloatBarrier

\section{Radsensor}
\label{sec_radsensor}

Das \gls{gls:aadc}-Modellauto verfügt über einen Radsensor, um  Bewegung, Position und Richtung der Vorderräder zu messen. Zudem kann dieser Sensor Raddrehzahlen messen, wobei Bruchteile von Radumdrehungen erkannt und so eine zurückgelegte Distanz zuverlässig und präzise berechnet werden. Die Versuche zeigten, dass eine Radumdrehung in 60 Bruchteilen gemessen wird. Da eine Radumdrehung exakt 33\,cm entsprechen, konnte daraus berechnet werden, dass $\frac{1}{60}$ einer Radumdrehung exakt 0,5\,cm sind. Da das Fahrzeug konstant geradeaus fährt, konnte vernachlässigt werden, dass Kurven diese Messungen eventuell verfälschen könnten.  Die gemessenen Distanzen dieses Sensors können also ohne Bedenken für die weiteren Berechnungen der Parklückensuche bzw. des Vermessens der Parklücke genutzt werden. Wie in \cref{txt:concept} erläutert, wird in \cref{sec_slotmeasuring} näher darauf eingegangen, welche Distanzen gewählt wurden und aus welchen Berechnungen diese hervorgingen.

Abschließend soll auch erwähnt werden, dass andere Methoden zur Distanzmessung berücksichtigt und so alle Möglichkeiten in Betracht gezogen wurden.  Andere Varianten, eine gefahrene Strecke zu messen, basieren jedoch auf dem Prinzip der Umrechnung von Geschwindigkeit und Zeit in Strecke. Dies erwies sich als problematisch, da hier die Geschwindigkeit des Fahrzeugs eingebunden werden musste, was dazu führte, dass Ungenauigkeiten im Messverhalten der Strecke auftraten, weil die vom Fahrzeug übermittelte Distanz oft ungenau war, da sie von äußeren Faktoren wie Widerstand und Reibung des Untergrunds verfälscht wurde.

\FloatBarrier

\section{Ultraschallsensoren}
\label{sec_ultraschallsensoren} 

Ultraschallsensoren (\gls{gls:uss}) sind als Abstandssensoren dazu geeignet, Hindernisse berührungslos zu erkennen und ihre Entfernung zum Sensor zu messen. Diese Sensoren sind in der Lage, Abstände von Hindernissen aus unterschiedlichsten Materialien, wie Metall, Holz oder Kunststoff zu erfassen. Lediglich schalldämpfende Stoffe, wie etwa Watte oder Stoffbezüge, aber auch schräg stehende Flächen können schlechter erfasst werden, da hier keine ausreichende Reflexion stattfinden kann.

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{Literatur/USS.png}
	\caption[Ultraschallsensor HC-SR04]{Ultraschallsensor HC-SR04, entnommen aus \cite{ussmanual2013}}
	\label{pic_ultraschallsensor}
\end{figure}

Der im Fahrzeug verbaute HC-SR04 Ultraschallsensor (siehe \cref{pic_ultraschallsensor}) eignet sich besonders, um Distanzen zwischen Modellauto und Objekten oder Hindernissen zu erkennen. Ultraschallsensoren arbeiten nach dem Prinzip der Laufzeitmessung von hochfrequenten Schallimpulsen. Dazu sendet der Sensor zyklisch einen solchen Impuls aus, welcher sich mit Schallgeschwindigkeit in der Luft ausbreitet. Sobald dieses Signal auf ein Hindernis trifft, wird es reflektiert. Dieses Echo wird von dem Sensor wieder erfasst und aus der Dauer, die das Signal zurück zum Sensor gebraucht hat, kann anschließend der Abstand zum erfassten Objekt berechnet werden. Innerhalb eines Winkels von \ang{30} kann er Entfernungen zwischen 2 und 400 cm erfassen \cite{manualaadc2017}. Er arbeitet mit einer Ultraschallfrequenz von 40\,kHz, was sich oberhalb der vom Menschen hörbaren Frequenz befindet. Relevant für die Präzision des Sensors ist die Auflösung von 0,3\,cm. Eine so geringe Auslösung kann im Rahmen der Messungen vernachlässigt werden, da die gemessenen Distanzen in einer Größenordnung sind, in dem eine solch kleine Abweichung kompensiert werden kann ohne Informationsverluste zu erleiden \cite{manualaadc2017}.

\pagebreak

Während den Versuchen zeigte sich, dass die Ultraschallsensoren sehr empfindlich und anfällig für Schwankungen und Ungenauigkeiten sind. Da die Abstandsmessungen zuverlässig funktionieren sollen, müssen die Sensordaten, bevor sie für die Komponenten benutzt werden können, aufbereitet werden. Die Sensoren weisen zwei grundsätzliche Fehler auf, zum einen das Schwanken der Entfernung, obwohl sich das Hindernis nicht bewegte und zum anderen das Nichterkennen eines Hindernisses. Letzteres führt zu einem Fehler, bei dem der Sensor kontinuierlich die Distanz \enquote{0} zurück gab, als ob sich ein Hindernis direkt vor dem Sensor befinden würde. Alle Fehler lassen sich durch eine Glättung der Sensordaten beheben.

\begin{listing}[h]
	\begin{algorithm}[H]
		
	
		\KwIn {Ultraschalldaten USData}
		\KwOut {Geglättete Ultraschalldaten}
	
		DataSet $\leftarrow$ $\emptyset$;\\
		\ForEach{DataPoint $\in$ USData}{ \If{DataPoint \neq 0}{DataSet $\leftarrow$ DataSet $\bigcup$ \{DataPoint\}}
		\If{DataSet.Length() >= 10}{smoothSignal $\leftarrow$ DataSet.getLast10().Average()}}
		\Return smoothSignal
	\end{algorithm}
	\caption{Das Glätten der Ultraschalldaten}
\label{code_smoothing}
\end{listing}

Die in \cref{code_smoothing} algorithmisch beschriebene Glättung der Sensordaten verläuft in zwei Schritten. Zu Beginn werden alle Null-Signale herausgefiltert, da hier davon ausgegangen werden kann, dass es sich um fehlerhafte Signale handelt, weil der Sensor nur für Distanzen zwischen 2 und 400\,cm konzipiert ist \cite{manualaadc2017}. In der zweiten Phase wird dann lediglich aus den 10 neusten Signalen ein Durchschnittswert gebildet, um das Signal so resistenter gegenüber Schwankungen zu machen. Der Ultraschallsensor sendet alle 20\,ms ein Signal, was 50 Signalen pro Sekunde entspricht. Daraus ergibt sich, dass eine Berechnung aus 10 Signalen zwei Zehntelsekunden dauert. Diese Dauer ist so kurz, dass sie die Reaktionszeit des Fahrzeugs nicht beeinflusst und daher vernachlässigbar ist. Auch das Herausfiltern der Null-Signale verlängert diese Messung nicht maßgeblich, da einerseits diese Null-Signale nur vereinzelt vorkommen und nach den ersten 10 gesammelten Signalen immer nur ein weiteres Signal für die nächste Durchschnittsberechnung benötigt wird.

\begin{figure}[b!]
	\centering
	\includegraphics[width=1\textwidth]{Literatur/ussvisualisationcutted.png}
	\caption[Visualisierung des Ultraschallsensors]{Visualisierung des Ultraschallsensors}
	\label{pic_ussvisualisation}
\end{figure}

In  \cref{pic_ussvisualisation} wird eine Visualisierung dieser Sensoren gezeigt. Bei dieser Visualisierung ist oben die Front des Autos und unten das Heck des Autos. Es ist zu erkennen, dass im Fahrzeug normalerweise 10 dieser Sensoren verbaut sind, die 5 Ultraschallsensoren an der Front fehlen, da das ursprünglich benutzte Fahrzeug gegen ein Fahrzeug ausgetauscht wurden, welches an dieser Stelle einen Licht- und Entfernungssensor besitzt (engl. light detection and ranging, \gls{gls:lidar}). Diese Visualisierung verdeutlicht, wie Distanzen gemessen werden. In \cref{pic_ussvisualisation} messen der linke und hintere linke Sensor beispielsweise Hindernisse in einer mittleren Entfernung. Die anderen drei angeschlossenen Sensoren hingegen können kein reflektiertes Ultraschallsignal empfangen und erkennen daher kein Hindernis in der näheren Umgebung. Wie in \cref{txt:concept} erläutert, ist diese Messung von Entfernungen für die weitere Entwicklung der Notbremse und der Parklückenvermessung relevant.

\FloatBarrier

\section{Inertiale Messeinheit}
\label{sec_imu}

Die inertiale Messeinheit (engl. inertial measurement unit, \gls{gls:imu}) ist ein Orientierungssensor zur Bestimmung von Position sowie Achsneigung oder Winkel des Fahrzeugs im Raum. Dieser Sensor ist in drei Teilsensoren unterteilt \cite[5 \psqq]{imu2016}. Zum einem dem Gyroskop, einem Kreiselinstrument, bei dem aufgrund der Stabilität der Kreiselachse die Lage innerhalb des Raumes berechnet werden. Ein Magnetometer, genauer ein Hall-Sensor, bildet den zweiten Teilsensor, welcher durch das Erfassen von magnetischen Flussdichten die Position und Neigung des Fahrzeugs innerhalb des Raumes misst. Ein weiter Teil dieses Sensors ist der Beschleunigungssensor. Dieser misst die Trägheitskraft auf eine Testmasse und kann so seine Beschleunigung berechnen. Somit kann eine Messung der Geschwindigkeitszunahme als auch -abnahme erfolgen.

Wie aus \cref{txt:concept} entnommen werden kann, wurde ein Sensor zur Positionsbestimmung benötigt, um die Einparkkurve im richtigen Winkel zu fahren. Im Verlauf der Versuche erwies sich die inertiale Messeinheit jedoch als ungeeignet. Die Positions- und Winkelbestimmung beruht auf einer Berechnung aus Magnetometer- und Beschleunigungsdaten. Es zeigte sich jedoch, dass das Magnetometer stark anfällig für magnetische Interferenz ist. Es können magnetische Verzerrungen auftreten, verursacht durch elektrische Störgrößen, Metallgeräte oder Metall in Gebäudestrukturen \cite{madgwick2010}.

Diese Beobachtungen wurden durch einige Versuche verifiziert. Dabei wurden Messungen an verschiedenen Stellen im Raum gemacht und die angegebene Position mit der tatsächlichen verglichen. Hierbei war zu erkennen, dass die übermittelte Positionsänderung maßgebliche Abweichungen aufwies, die so stark waren, dass auch eine Glättung oder eine Anpassung der Sensordaten nicht realisierbar war. Diese Beobachtungen konnten auch außerhalb des Gebäudes, in dem die magnetische Interferenz hätte deutlich niedriger sein müssen, wiederholt werden, was die Erkenntnis der Unbrauchbarkeit des Sensors weiter stärkte.

Obwohl dieser Sensor durch seine Fähigkeiten prädestiniert für diesen Zweck war, konnte er letztendlich nicht bei der Entwicklung verwendet werden und es musste eine Alternative zur Positionsbestimmung gefunden werden.

\FloatBarrier

\section{Light detection and ranging}
\label{sec_lidar}

Der RPLIDAR A2 \ang{360} Laserscanner ist ein Sensor, der ähnlich wie der Ultraschallsensor auf Laufzeitmessung basiert. Hierbei werden Laserimpulse in die Umgebung gesendet und der Sensor nimmt das zurückgestreute Licht wieder auf. Anhand der Dauer, der sogenannten Lichtlaufzeit der Signale kann die Entfernung zum Ort der Reflexion berechnet werden. Der Sensor kann Entfernungen zwischen 15\,cm und 16\,m erkennen \cite{slamtec2019}.

\begin{figure}[b!]
	\centering
	\includegraphics[width=0.5\textwidth]{Literatur/rplidar.jpg}
	\caption[RPLIDAR A2 \ang{360} Laserscanner]{RPLIDAR A2 \ang{360} Laserscanner, entnommen aus \cite{digitalwerk2017}}
	\label{pic_rplidar}
\end{figure}
\label{key}
In \cref{pic_rplidar} ist ein solcher, im Modellauto verbauter, Sensor zu sehen. Dieser Sensor gehört dank seines Lasertriangulations-Messsystems zu den präzisesten Sensoren. Er liefert nicht nur in Innenräumen, sondern auch in Außenbereichen ohne direkte Sonneneinstrahlung exakte Werte \cite{digitalwerk2017}. Da dieser Sensor so genau, sicher und resistent gegenüber Umweltfaktoren ist, stellt er einen geeigneten Ersatz für die \gls{gls:imu} dar. In \cref{sec_imu} konnte gezeigt werden, dass die inertiale Messeinheit aufgrund von magnetischer Interferenz nicht für den weiteren Gebrauch geeignet war. Wie in \cref{txt:concept} erwähnt, erfordert das in dieser Arbeit entwickelte Assistenzsystem eine zuverlässige und sichere Methode zur Positions- und Winkelbestimmung des Fahrzeugs.

\begin{figure}[b!]
	\centering
	\includegraphics[width=1\textwidth]{Literatur/lidarvisualisation.png}
	\caption[Visualisierung des \gls{gls:lidar}-Sensors]{Visualisierung des \gls{gls:lidar}-Sensors}
	\label{pic_lidarvisualisation}
\end{figure}

Anhand von \cref{pic_lidarvisualisation} kann näher verstanden werden, wie ein solcher \gls{gls:lidar}-Sensor Hindernisse erkennt. Das blaue Rechteck stellt dabei das Fahrzeug dar, wobei rechts die Front des Modellautos ist. Die roten Linien, genauer die roten Punkte, sind Stellen, an denen eine Reflexion wahrgenommen wurde. Bei der Visualisierung kann ein Koordinatensystem unterlegt werden, um die gemessenen Distanzen besser deuten zu können. In dieser Abbildung wurde aus Gründen der Übersicht und der Verständlichkeit darauf verzichtet.

In \cref{sec_imu} wurde bereits erläutert, dass das Fahrzeug eine Positionsbestimmung benötigt, um die Einparkkurve im richtigen Winkel zu fahren und dass die \gls{gls:imu}-Daten aufgrund der magnetischen Interferenz nicht dazu benutzt werden können. Im Folgenden wird nun erörtert, wie aus diesem \gls{gls:lidar}-Sensor solche Daten synthetisiert werden können, um diese Daten zu ersetzen.

Damit zunächst verstanden werden kann, auf welchen Prinzipien die Einparkkurve beruht muss geklärt werden, wie das Modellauto die Umwelt wahrnehmen kann. Um die richtige Kurve zu fahren, muss das Fahrzeug einen gewissen Winkel zur Straße haben. Wie in \cref{sec_lidar} verdeutlicht, benutzt dieser Sensor Laserimpulse, um Hindernisse zu erkennen. Weil zu Beginn der Einparkkurve davon ausgegangen werden kann, dass sich das Fahrzeug parallel zum Fahrbahnrand befindet, kann gefolgert werden, dass sich das Fahrzeug senkrecht zum Horizont befindet. Dieser Horizont wurde fiktiv erzeugt, da die eigentliche Funktion des \gls{gls:lidar}-Sensors nicht darauf ausgelegt ist, einen solchen zu erkennen. Dazu wird ein Hindernis vor der Parklücke positioniert, welches eben, breit und hoch genug sein muss, um vom Sensor erkannt zu werden. In der Entwicklung wurde das dadurch realisiert, dass das Fahrzeug, nachdem es die freie Parklücke gefunden hat, anhält und nun vor einer Wand steht.

Folglich muss erörtert werden, wie aus diesem fiktiven Horizont Daten zur Winkelbestimmung gewonnen werden können. In \cref{pic_lidarvisualisation} ist zu erkennen, wie der Sensor Hindernisse wahrnimmt. Der Sensor liefert für jeden reflektierten Impuls dessen Koordinaten im zweidimensionalen Raum. Es wird für die Entfernung zum Fahrzeug die X-Koordinate und für die Verschiebung von Links und Rechts jeweils die Y-Koordinate übermittelt. Hierbei stellt der Sensor den Punkt (0,0) dar. Kleine Distanzen kann der Sensor dabei nicht wahrnehmen; diese Grenze liegt für die X-Werte bei unter 20\,cm. Dies muss berücksichtigt werden, damit das Fahrzeug nicht zu nah an der Wand steht und so keine Messung stattfinden kann. Da jeder reflektierte Laserimpuls als Hindernis erkannt wird, erkennt dieser Sensor bei der Wand also eine Reihe von Punkten, die den gleichen Abstand zum Fahrzeug haben. Mit Hilfe von linearer Regression, soll aus dieser Wand eine gerade erkannt werden. \cref{table_lineareRegression} zeigt, welche Werte für eine solche lineare Regression oder die Regressionsgerade benötigt werden. Diese Gerade stellt für die weitere Entwicklung den fiktiven Horizont dar. Dieser Horizont soll dabei eine Orientierungshilfe für das Fahrzeug sein, aus welcher der Winkel zwischen Fahrzeug und Straße berechnet wird.
Lineare Regression ist dabei ein mathematisches Prinzip, bei dem mehrere Punkte dazu verwendet werden eine Gerade zu definieren. Die einzelnen Lichtreflexionen bilden dabei die Punkte, die für dieses Verfahren verwendet werden.

\begin{table}
	\begin{tabular}{ l p{6cm} p{5cm}}

		\textbf{Parameter}  & \textbf{Beschreibung} & \textbf{Berechnung}\\
		\hline
		\vspace{5mm}
		$\bar{x}$ & Arithmetisches Mittel aller X-Werte & $\bar{x} =\frac{i=1}{n} \cdot \sum_{1}^{n} x_i$\\
		\vspace{5mm}
		$\bar{y}$ & Arithmetisches Mittel aller Y-Werte & $\bar{y} =\frac{i=1}{n} \cdot \sum_{1}^{n} y_i$\\
		\vspace{5mm}
		$S_{xy}$ & Summe der quadratischen Abweichung: X und Y & $S_{xy} = \sum_{i=1}^{n} (x_i - \bar{x}) \cdot (y_i - \bar{y})$\\
		\vspace{5mm}
		$S_{xx}$ & Summe der quadratischen Abweichung: X & $S_{xx} = \sum_{i=1}^{n} (x_i - \bar{x})²$\\
		\vspace{5mm}
		m & Steigung der Regressionsgeraden & $m=\frac{S_{xy}}{S_{xx}}$\\
		

	\end{tabular}
		\caption{Benötigte Werte und Berechnungen der linearen Regression}
\label{table_lineareRegression}
\end{table}

Eine Geradengleichung besteht immer aus den gleichen Komponenten. Eine solche Gleichung hat immer die Form $y=m \cdot x+b$, wobei m die Steigung der Geraden ist und b der Y-Wert, in dem die Gerade die Y-Achse schneidet. X und Y seien jeweils die Koordinaten eines Punktes. Dabei kann die Steigung m niemals 0 betragen, das würde heißen, dass die Wand auf der X-Achse steht. Der Sensor könnte hierbei nicht die hintereinander liegenden Reflexionen wahrnehmen, da er selber auf der X Achse liegt.

Für die Entwicklung des Horizonts ist lediglich die Steigung m der Geraden relevant, da nur anhand der Veränderung des Winkels zwischen Wand und Fahrzeug das aktuelle Fahrmanöver bestimmt werden soll. Sobald diese Steigung m ermittelt wurde, kann der Winkel zwischen Wand und Fahrzeug berechnet werden, dazu mus lediglich die Steigung in die Formel $\alpha$ = $\arctan(m) \cdot \frac{180}{\pi}$ eingesetzt werden. Der Winkel, der zu Beginn des Einparkvorgangs berechnet wurde ist der Startwinkel. Die folgenden Winkel werden durch die Differenz zu diesem Winkel bestimmt.

Zwischen dem \gls{gls:lidar}-Sensor und der Einparkkomponente befinden sich zwei Zwischenschritte. Der erste ist der oben erklärte Vorgang, bei dem durch die Wand, also dem Horizont, der aktuelle Winkel zum Fahrzeug bestimmt wird. Im zweiten Schritt wird während des Einparkvorgangs kontinuierlich der aktuelle Winkel berechnet. Aus diesen zwei Schritten werden die \gls{gls:imu}-Daten simuliert.

Der \gls{gls:lidar}-Sensor stellt also einen Ersatz zur inertialen Messeinheit dar. In \cref{sec_imu} werden die bekannten Probleme, die ein solcher Sensor in magnetischen Umfeldern hat, aufgezeigt. Wie aus diesen Daten eine Winkelberechnung zwischen Fahrzeug und Straße erfolgen kann, wird in \cref{sec_parkingprocess} \enquote{Der Einparkvorgang} genauer erörtert. Diese Komponente basiert wie in \cref{txt:concept} verdeutlicht, auf einer genauen Positionsbestimmung, um eine möglichst exakte Einparkkurve fahren zu können.

\FloatBarrier