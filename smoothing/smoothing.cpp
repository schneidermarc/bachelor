//
// Created by maschn on 02.05.19.
//
#include "stdafx.h"
#include "smoothing.h"
#include "../utils/structs.h"
#include "aadc_structs.h"


ADTF_FILTER_PLUGIN(DISPLAY_FILTER_NAME, OID_ADTF_SMOOTHING, smoothing);

smoothing::smoothing(const tChar *__info) : cFilter(__info) {
}

smoothing::~smoothing() {

}

tResult smoothing::Init(tInitStage eStage, __exception) {

    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult smoothing::registerInputPins(__exception) {
    THROW_IF_FAILED(ussStructInputPin.Create("USSDataIn", "tUltrasonicStruct", static_cast<IPinEventSink *>(this),
                                             __exception_ptr));
    THROW_IF_FAILED(RegisterPin(&ussStructInputPin));

    RETURN_NOERROR;
}

tResult smoothing::registerOutputPins(__exception) {
    THROW_IF_FAILED(ussStructOutputPin.Create("USSDataOut", "tUltrasonicStruct", static_cast<IPinEventSink *>(this),
                                              __exception_ptr));
    THROW_IF_FAILED(RegisterPin(&ussStructOutputPin));

    RETURN_NOERROR;
}

tResult smoothing::Shutdown(tInitStage eStage, __exception) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);

}

tResult smoothing::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                                 IMediaSample *pMediaSample) {

    if (nEventCode == IPinEventSink::PE_MediaSampleReceived) {
        // Differ between PINs
        if (pSource == &ussStructInputPin) {

            tUltrasonicStruct ultrasonicData;
            __sample_read_lock(pMediaSample, tUltrasonicStruct, pData);
            ultrasonicData = *pData;

            BufferUSSData(ultrasonicData);
            if(rearRightBuffer.size() > 7) {
                TransmitSmoothUSSSignal();
            }



       /*     if (IS_OK(pMediaSample->Lock((const tVoid **) &pSampleData))) {
                tSignalValue tFrontLeft;
                tSignalValue tFrontCenterLeft;
                tSignalValue tFrontCenter;
                tSignalValue tFrontCenterRight;
                tSignalValue tFrontRight;
                tSignalValue tSideLeft;
                tSignalValue tSideRight;
                tSignalValue tRearLeft;
                tSignalValue tRearCenter;
                tSignalValue tRearRight;

                tFrontLeft.f32Value = pSampleData->tFrontLeft.f32Value;
                tFrontCenterLeft.f32Value = 0;
                tFrontCenter.f32Value = 100;
                tFrontCenterRight.f32Value = 1;

                tUltrasonicStruct* ussStruct = new tUltrasonicStruct{tFrontLeft, tFrontCenterLeft, tFrontCenter, tFrontCenterRight, tFrontRight, tSideLeft, tSideRight, tRearLeft, tRearCenter, tRearRight};

                cObjectPtr<IMediaSample> smoothSample;
                RETURN_IF_FAILED(AllocMediaSample((tVoid**)&smoothSample));
                RETURN_IF_FAILED(smoothSample->Update(_clock->GetStreamTime(), &ussStruct, sizeof(ussStruct), IMediaSample::MSF_None));

                RETURN_IF_FAILED(ussStructOutputPin.Transmit(smoothSample));
            }

        */

        }
    }
    RETURN_NOERROR;
}

tResult smoothing::BufferUSSData(tUltrasonicStruct ultrasonicData) {

    frontLeftBuffer.push_back(ultrasonicData.tFrontLeft.f32Value);
    frontCenterLeftBuffer.push_back(ultrasonicData.tFrontCenterLeft.f32Value);
    frontCenterBuffer.push_back(ultrasonicData.tFrontCenter.f32Value);
    frontCenterRightBuffer.push_back(ultrasonicData.tFrontCenterRight.f32Value);
    frontRightBuffer.push_back(ultrasonicData.tFrontRight.f32Value);
    sideLeftBuffer.push_back(ultrasonicData.tSideLeft.f32Value);
    if(ultrasonicData.tSideRight.f32Value > 5) {
        sideRightBuffer.push_back(ultrasonicData.tSideRight.f32Value);
    }
    rearCenterBuffer.push_back(ultrasonicData.tRearCenter.f32Value);
    rearLeftBuffer.push_back(ultrasonicData.tRearLeft.f32Value);
    rearRightBuffer.push_back(ultrasonicData.tRearRight.f32Value);


    lastTimestamp = ultrasonicData.tFrontLeft.ui32ArduinoTimestamp;

    RETURN_NOERROR;
}

tFloat32 smoothing::MovingAverage(boost::circular_buffer<tFloat32> values){
    float acc, mean= 0.0;
    int validCount = 0;

    // filter out any values looking like noise
    for (int i = 0; i < 5; i++) {
        if ((values[i] > 0) & (values[i] < 399)) {
            acc += values[i];
            validCount++;
        }
    }
    // if we had no valid values, return an error value
    if (validCount == 0){
        mean = -1.0;
    } else{
        mean = acc / validCount;
    }

    //tFloat32 mean = std::accumulate(std::begin(values), std::end(values), 0.0) / bufferSize;
    return(mean);
}

tResult smoothing::TransmitSmoothUSSSignal() {

    tSignalValue tFrontLeft {lastTimestamp ,MovingAverage(frontLeftBuffer)};
    tSignalValue tFrontCenterLeft {lastTimestamp ,MovingAverage(frontCenterLeftBuffer)};
    tSignalValue tFrontCenter {lastTimestamp ,MovingAverage(frontCenterBuffer)};
    tSignalValue tFrontCenterRight {lastTimestamp ,MovingAverage(frontCenterRightBuffer)};
    tSignalValue tFrontRight {lastTimestamp ,MovingAverage(frontRightBuffer)};
    tSignalValue tSideLeft {lastTimestamp ,MovingAverage(sideLeftBuffer)};
    tSignalValue tSideRight {lastTimestamp ,MovingAverage(sideRightBuffer)};
    tSignalValue tRearLeft {lastTimestamp ,MovingAverage(rearLeftBuffer)};
    tSignalValue tRearCenter {lastTimestamp ,MovingAverage(rearCenterBuffer)};
    tSignalValue tRearRight {lastTimestamp ,MovingAverage(rearRightBuffer)};

    tUltrasonicStruct ussStruct{tFrontLeft, tFrontCenterLeft, tFrontCenter, tFrontCenterRight, tFrontRight, tSideLeft, tSideRight, tRearLeft, tRearCenter, tRearRight};

    cObjectPtr<IMediaSample> smoothSample;
    RETURN_IF_FAILED(AllocMediaSample((tVoid**)&smoothSample));
    RETURN_IF_FAILED(smoothSample->Update(_clock->GetStreamTime(), &ussStruct, sizeof(ussStruct), IMediaSample::MSF_None));

    RETURN_IF_FAILED(ussStructOutputPin.Transmit(smoothSample));

    RETURN_NOERROR;
}