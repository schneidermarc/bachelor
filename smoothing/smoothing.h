//
// Created by maschn on 02.05.19.
//

#ifndef AADC_USER_SMOOTHING_H
#define AADC_USER_SMOOTHING_H

#include "stdafx.h"
#include "../adtf_extensions/MediaTypeInputPin.h"
#include "../adtf_extensions/MediaTypeOutputPin.h"
#include "boost/circular_buffer.hpp"
#include "../utils/structs.h"
#include "aadc_structs.h"

#define OID_ADTF_SMOOTHING "adtf.example.smoothing"
#define DISPLAY_FILTER_NAME "Smoothing"

class smoothing : public adtf::cFilter
{
/*! set the filter ID and the version */
ADTF_FILTER(OID_ADTF_SMOOTHING, "smoothing", adtf::OBJCAT_DataFilter);

protected:

    // input PINs:
    MediaTypeInputPin ussStructInputPin;

    // output PINs:
    MediaTypeOutputPin ussStructOutputPin;

public:

    smoothing(const tChar* __info);

    virtual ~smoothing();

protected:

    // Custom PIN registration methods:
    tResult registerInputPins(__exception);

    tResult registerOutputPins(__exception);



    tResult Init(tInitStage eStage, ucom::IException** __exception_ptr);

    tResult Shutdown(tInitStage eStage, ucom::IException** __exception_ptr = NULL);

    tResult OnPinEvent(IPin* pSource, tInt nEventCode, tInt nParam1, tInt nParam2, IMediaSample* pMediaSample);


  //  tFloat32 EWMA(boost::circular_buffer<tFloat32> values);
    tFloat32 MovingAverage(boost::circular_buffer<tFloat32> values);
    tResult BufferUSSData(tUltrasonicStruct ultrasonicData);
    tResult TransmitSmoothUSSSignal();

    tUInt8 bufferSize = 20;
    tUInt32 lastTimestamp;
    boost::circular_buffer<tFloat32> frontLeftBuffer{bufferSize};
    boost::circular_buffer<tFloat32> frontCenterLeftBuffer{bufferSize};
    boost::circular_buffer<tFloat32> frontCenterBuffer{bufferSize};
    boost::circular_buffer<tFloat32> frontCenterRightBuffer{bufferSize};
    boost::circular_buffer<tFloat32> frontRightBuffer{bufferSize};
    boost::circular_buffer<tFloat32> sideLeftBuffer{bufferSize};
    boost::circular_buffer<tFloat32> sideRightBuffer{bufferSize};
    boost::circular_buffer<tFloat32> rearLeftBuffer{bufferSize};
    boost::circular_buffer<tFloat32> rearCenterBuffer{bufferSize};
    boost::circular_buffer<tFloat32> rearRightBuffer{bufferSize};


 //   int wSize;
};



#endif //AADC_USER_SMOOTHING_H
