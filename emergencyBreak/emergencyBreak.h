//
// Created by maschn on 07.05.19.
//

#ifndef AADC_USER_EMERGENCYBREAK_H
#define AADC_USER_EMERGENCYBREAK_H

#include "stdafx.h"
#include "../adtf_extensions/MediaTypeInputPin.h"
#include "../adtf_extensions/MediaTypeOutputPin.h"
#include "../utils/structs.h"
#include "aadc_structs.h"

#define OID_ADTF_EmergencyBreak "adtf.example.emergencyBreak"
#define DISPLAY_FILTER_NAME "emergencyBreak"

class emergencyBreak : public adtf::cFilter
{

    /*! set the filter ID and the version */
    ADTF_FILTER(OID_ADTF_EmergencyBreak, "emergencyBreak", adtf::OBJCAT_DataFilter);

public:

    // input PIN:
    MediaTypeInputPin ussStructInputPin;
    cInputPin lidar_in;
    // output PIN:
    cOutputPin emergencyOutput;

    emergencyBreak(const tChar* __info);
    virtual ~emergencyBreak();

protected:

    // Custom PIN registration methods:
    tResult registerInputPins(__exception);
    tResult registerOutputPins(__exception);

    tResult Init(tInitStage eStage, ucom::IException** __exception_ptr);
    tResult Shutdown(tInitStage eStage, ucom::IException** __exception_ptr = NULL);
    tResult OnPinEvent(IPin* pSource, tInt nEventCode, tInt nParam1, tInt nParam2, IMediaSample* pMediaSample);

private:

    struct tProperties
    {
        tFloat32 fMinDistance;

        tProperties()
        {
            fMinDistance = 20.0;
        }
    } m_sProperties;

    cObjectPtr<IMediaTypeDescription> m_pCoderDescSignal;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutputB;
    cObjectPtr<IMediaTypeDescription> m_pDescriptionFloat;

    cObjectPtr<IMediaTypeDescription> m_pDescriptionBool;

    cObjectPtr<IMediaTypeDescription> m_pDescriptionEmergencyStop;
    bool emergency;

};


#endif //AADC_USER_EMERGENCYBREAK_H
