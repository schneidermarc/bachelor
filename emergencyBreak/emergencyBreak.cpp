//
// Created by maschn on 07.05.19.
//
#include "stdafx.h"
#include "emergencyBreak.h"
#include "aadc_structs.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include<math.h>
#include "adtf_graphics.h"
#include "chrono"
using namespace adtf_graphics;

ADTF_FILTER_PLUGIN(DISPLAY_FILTER_NAME, OID_ADTF_EmergencyBreak, emergencyBreak);

#define PROP_MIN_DISTANCE "Minimal Distance"

emergencyBreak::emergencyBreak(const tChar *__info)  : cFilter(__info){
    SetPropertyInt(PROP_MIN_DISTANCE, m_sProperties.fMinDistance);
    emergency = false;
}

emergencyBreak::~emergencyBreak() {

}

tResult emergencyBreak::Init(cFilter::tInitStage eStage, ucom::IException **__exception_ptr) {
    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            m_sProperties.fMinDistance = GetPropertyInt(PROP_MIN_DISTANCE);
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult emergencyBreak::registerInputPins(IException **__exception_ptr) {
    THROW_IF_FAILED(ussStructInputPin.Create("USSDataIn", "tUltrasonicStruct", static_cast<IPinEventSink *>(this),
                                             __exception_ptr));
    THROW_IF_FAILED(RegisterPin(&ussStructInputPin));

    cObjectPtr<IMediaType> pInputType;
    RETURN_IF_FAILED(AllocMediaType(&pInputType, MEDIA_TYPE_STRUCTURED_DATA, MEDIA_SUBTYPE_STRUCT_STRUCTURED, __exception_ptr));

    RETURN_IF_FAILED(lidar_in.Create("input_rplidar", pInputType, this));
    RETURN_IF_FAILED(RegisterPin(&lidar_in));


    RETURN_NOERROR;
}

tResult emergencyBreak::registerOutputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(emergencyOutput.Create("HazardLight", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&emergencyOutput));

    RETURN_NOERROR;
}

tResult emergencyBreak::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                                   IMediaSample *pMediaSample) {
    if (nEventCode == IPinEventSink::PE_MediaSampleReceived) {
        tUltrasonicStruct ultrasonicData;

        tUInt32 timestamp{0};

        RETURN_IF_POINTER_NULL(pMediaSample);
        // Differ between PINs
        if (pSource == &ussStructInputPin) {

            __sample_read_lock(pMediaSample, tUltrasonicStruct, pData);
            ultrasonicData = *pData;
            tFloat32 sideLeftDistance = ultrasonicData.tSideLeft.f32Value;
           // tFloat32 sideRightDistance = ultrasonicData.tSideRight.f32Value;
            tFloat32 rearLeftDistance = ultrasonicData.tRearLeft.f32Value;
            tFloat32 rearCenterDistance = ultrasonicData.tRearCenter.f32Value;
            tFloat32 rearRightDistance = ultrasonicData.tRearRight.f32Value;
            if(/*sideLeftDistance <= 6 ||*/ rearRightDistance <= 15 || rearLeftDistance <= 15 || rearCenterDistance <= 15){
                emergency = true;
                LOG_ERROR("USS");
            }
        }
        if (pSource == &lidar_in) {
      /*      cPointExt <tFloat64> *l_pSrcBuffer;
            std::vector<cPointExt<tFloat64>> m_vpoints;
            std::vector<cPointExt<tFloat64>> temp;
            if (IS_OK(pMediaSample->Lock((const tVoid **) &l_pSrcBuffer))) {
                m_vpoints.clear();
                m_vpoints.assign(l_pSrcBuffer,
                           l_pSrcBuffer + pMediaSample->GetSize() / sizeof(cPointExt<tFloat64>));
                copy_if(m_vpoints.begin(), m_vpoints.end(), std::back_inserter(temp),
                [](cPointExt<tFloat64> i) {
                return i.GetXValue() != 0 && i.GetYValue() != 0 && i.GetXValue() < 150 &&
                                   i.GetYValue() >= -250 && i.GetYValue() <= 250;
                });
                m_vpoints = temp;
                std::stringstream ss;
                ss << "{\"points\":[";

                for (auto & m_vpoint : m_vpoints) {
                     ss << "[" << m_vpoint.GetX() << "," << m_vpoint.GetY() << "],";
                }

                std::string str = ss.str();
                str.resize(str.size() - 1);
                str += "]}";

                std::string str2 = str;
                const char *pr = str2.c_str();
                LOG_ERROR(pr);

                if(!m_vpoints.empty()){
                    emergency = true;
                    LOG_ERROR("LIDAR");
                }
                pMediaSample->Unlock((const tVoid **) &l_pSrcBuffer);
            }*/
        }
        if(emergency){
            /********  BOOL OUTPUT FOR EMERGENCYBREAK (tjuryemergencystop)  ********/
            tBool bEmergencyStop{tTrue};
            tBoolSignalValue enableBreak{timestamp,bEmergencyStop};

            cObjectPtr<IMediaSample> pEmergency;
            RETURN_IF_FAILED(AllocMediaSample((tVoid**)&pEmergency));
            RETURN_IF_FAILED(pEmergency->Update(_clock->GetStreamTime(), &enableBreak, sizeof(enableBreak), IMediaSample::MSF_None));

            RETURN_IF_FAILED(emergencyOutput.Transmit(pEmergency));

        } else {
            tBool bEmergencyStop{tFalse};
            tBoolSignalValue enableBreak{timestamp,bEmergencyStop};

            cObjectPtr<IMediaSample> pEmergency;
            RETURN_IF_FAILED(AllocMediaSample((tVoid**)&pEmergency));
            RETURN_IF_FAILED(pEmergency->Update(_clock->GetStreamTime(), &enableBreak, sizeof(enableBreak), IMediaSample::MSF_None));

            RETURN_IF_FAILED(emergencyOutput.Transmit(pEmergency));
        }

    }
    RETURN_NOERROR;
}

tResult emergencyBreak::Shutdown(cFilter::tInitStage eStage, IException **__exception_ptr) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);
}