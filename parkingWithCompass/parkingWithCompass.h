//
// Created by maschn on 12.06.19.
//

#ifndef AADC_USER_PARKINGWITHCOMPASS_H
#define AADC_USER_PARKINGWITHCOMPASS_H

#include "stdafx.h"
#include "aadc_structs.h"
#define OID_ADTF_FILTER_NAME "adtf.example.parkingWithCompass"
#define DISPLAY_FILTER_NAME "parkingWithCompass"


class ParkingWithCompass : public adtf::cFilter {
    ADTF_FILTER(OID_ADTF_FILTER_NAME, DISPLAY_FILTER_NAME, adtf::OBJCAT_DataFilter);


protected:
    cInputPin angleIn;
    cInputPin enabler;
    cInputPin speedInput;
    cInputPin steeringInput;
    cInputPin emergencyInput;

    cOutputPin steeringOutput;
    cOutputPin speedOutput;

public:
    ParkingWithCompass(const tChar* __info);
    virtual ~ParkingWithCompass();

protected:
    // Custom PIN registration methods:
    tResult registerInputPins(__exception);
    tResult registerOutputPins(__exception);

    tResult Init(tInitStage eStage, ucom::IException** __exception_ptr);
    tResult Shutdown(tInitStage eStage, ucom::IException** __exception_ptr = NULL);
    tResult OnPinEvent(IPin* pSource, tInt nEventCode, tInt nParam1, tInt nParam2, IMediaSample* pMediaSample);

    tResult calculateCurve(tSignalValue angleData,tSignalValue speedData, tSignalValue steeringData);

private:
    cObjectPtr<IMediaTypeDescription> m_pDescriptionInerMeasUnitData;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutput;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescSignal;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutputB;

    tBoolSignalValue enablerData;
    tBoolSignalValue emergencyData;
    tSignalValue angleData;
    tSignalValue speedData;
    tSignalValue steeringData;

    bool startAngleTaken;
    tFloat32 angle;
    bool enabled;
    bool emergency;

    bool phaseZero;
    bool phaseOne;
    bool phaseTwo;
    bool phaseThree;
};



#endif //AADC_USER_PARKINGWITHCOMPASS_H
