//
// Created by maschn on 12.06.19.
//
#include "parkingWithCompass.h"
#include <sstream>
#include "iostream"
#include <chrono>
#include <thread>
using namespace std::this_thread;
using namespace std::literals::chrono_literals;
using std::chrono::system_clock;

ADTF_FILTER_PLUGIN(DISPLAY_FILTER_NAME, OID_ADTF_FILTER_NAME, ParkingWithCompass);

ParkingWithCompass::ParkingWithCompass(const tChar *__info)  : cFilter(__info){
    phaseZero = true;
    phaseOne = false;
    phaseTwo = false;
    phaseThree = false;
    startAngleTaken = false;
    enabled = false;
    emergency = false;
}

ParkingWithCompass::~ParkingWithCompass() {

}

tResult ParkingWithCompass::Init(cFilter::tInitStage eStage, ucom::IException **__exception_ptr) {
    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult ParkingWithCompass::registerInputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(angleIn.Create("angleInput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&angleIn));

    RETURN_IF_FAILED(speedInput.Create("speedInput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&speedInput));

    RETURN_IF_FAILED(steeringInput.Create("steeringInput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&steeringInput));

    RETURN_IF_FAILED(enabler.Create("Enabler", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&enabler));

    RETURN_IF_FAILED(emergencyInput.Create("EmergencyInput", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&emergencyInput));


    RETURN_NOERROR;
}

tResult ParkingWithCompass::registerOutputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER,
                                         (tVoid **) &pDescManager, __exception_ptr));

    tChar const *strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0, 0, 0, "tSignalValue", strDescSignalValue,
                                                             IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid **) &m_pCoderDescOutput));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));


    RETURN_IF_FAILED(steeringOutput.Create("steeringOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&steeringOutput));

    RETURN_IF_FAILED(speedOutput.Create("speedOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&speedOutput));

    RETURN_NOERROR;
}

tResult ParkingWithCompass::Shutdown(cFilter::tInitStage eStage, IException **__exception_ptr) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);
}

tResult ParkingWithCompass::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                                       IMediaSample *pMediaSample) {
    if (nEventCode == IPinEventSink::PE_MediaSampleReceived) {

        RETURN_IF_POINTER_NULL(pMediaSample);



        // Differ between PINs
        if(pSource == &enabler){
            __sample_read_lock(pMediaSample, tBoolSignalValue, pData);
            enablerData = *pData;

            if(enablerData.bValue == tTrue) {
                enabled = true;
            }
        }

        if (pSource == &angleIn) {
            __sample_read_lock(pMediaSample, tSignalValue, pData);
            angleData = *pData;
        }

        if (pSource == &speedInput){
            __sample_read_lock(pMediaSample, tSignalValue, pData);
            speedData = *pData;
        }

        if (pSource == &steeringInput){
            __sample_read_lock(pMediaSample, tSignalValue, pData);
            steeringData = *pData;
        }

        if(pSource == &emergencyInput){
            __sample_read_lock(pMediaSample, tBoolSignalValue, pData);
            emergencyData = *pData;

            if(emergencyData.bValue == tTrue){
                emergency = true;
            } else {
                emergency = false;
            }
        }

        if(emergency){
            tUInt32 timestamp{0};
            tFloat32 steering{0};
            tFloat32 speed{0};
            tSignalValue steeringValue{timestamp, steering};
            tSignalValue speedValue{timestamp, speed};

            cObjectPtr<IMediaSample> steeringSample;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &steeringSample));
            RETURN_IF_FAILED(steeringSample->Update(_clock->GetStreamTime(), &steeringValue, sizeof(steeringValue),
                                                    IMediaSample::MSF_None));

            RETURN_IF_FAILED(steeringOutput.Transmit(steeringSample));

            cObjectPtr<IMediaSample> speedSample;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &speedSample));
            RETURN_IF_FAILED(
                    speedSample->Update(_clock->GetStreamTime(), &speedValue, sizeof(speedValue), IMediaSample::MSF_None));

            RETURN_IF_FAILED(speedOutput.Transmit(speedSample));
        }else if(enabled && !emergency){
            calculateCurve(angleData,speedData ,steeringData);
        } else if (!enabled && !emergency){
            cObjectPtr<IMediaSample> steeringSample;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &steeringSample));
            RETURN_IF_FAILED(steeringSample->Update(_clock->GetStreamTime(), &steeringData, sizeof(steeringData),
                                                    IMediaSample::MSF_None));

            RETURN_IF_FAILED(steeringOutput.Transmit(steeringSample));

            cObjectPtr<IMediaSample> speedSample;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &speedSample));
            RETURN_IF_FAILED(
                    speedSample->Update(_clock->GetStreamTime(), &speedData, sizeof(speedData), IMediaSample::MSF_None));

            RETURN_IF_FAILED(speedOutput.Transmit(speedSample));
         //   LOG_ERROR("OLD VALUES");
            RETURN_NOERROR;
        }

    }
    RETURN_NOERROR;
}

tResult ParkingWithCompass::calculateCurve(tSignalValue angleData,tSignalValue speedData, tSignalValue steeringData) {
    tUInt32 timestamp = speedData.ui32ArduinoTimestamp;
    tFloat32 steering{0};
    tFloat32 speed{0};

    if(angleData.f32Value != 0) {
        angle = angleData.f32Value;
    }

    std::stringstream ss;
    ss << "ANGLE: " << angle;
    std::string angleString = ss.str();
    const char *result = angleString.c_str();
    LOG_ERROR(result);

        if (angle != 0 &&angle < 5 && phaseZero) {
            steering = 100;
            speed = 13;
            phaseOne = true;
            LOG_ERROR("WINKEL < 3");
        } else if (angle != 0 &&(angle < 40) && phaseOne) {
            steering = 100;
            speed = 13;
            phaseZero = false;
            phaseTwo = true;
            LOG_ERROR("RECHTS LENKEN UND ZURÜCK");
        } else if (angle != 0 &&(angle > 3) && phaseTwo) {
            steering = -100;
            speed = 13;
            phaseOne = false;
            phaseThree = true;
            LOG_ERROR("LINKS LENKEN UND ZURÜCK");
        } else if (angle != 0 &&(angle < 3) && phaseThree) {
            steering = 0;
            speed = 0;
            phaseTwo = false;
            LOG_ERROR("STEHE IN LÜCKE");
        }

    tSignalValue steeringValue{timestamp, steering};

    cObjectPtr<IMediaSample> steeringSample;
    RETURN_IF_FAILED(AllocMediaSample((tVoid **) &steeringSample));
    RETURN_IF_FAILED(steeringSample->Update(_clock->GetStreamTime(), &steeringValue, sizeof(steeringValue),
                                            IMediaSample::MSF_None));

    RETURN_IF_FAILED(steeringOutput.Transmit(steeringSample));

    tSignalValue speedValue{timestamp, speed};

    cObjectPtr<IMediaSample> speedSample;
    RETURN_IF_FAILED(AllocMediaSample((tVoid **) &speedSample));
    RETURN_IF_FAILED(
            speedSample->Update(_clock->GetStreamTime(), &speedValue, sizeof(speedValue), IMediaSample::MSF_None));

    RETURN_IF_FAILED(speedOutput.Transmit(speedSample));
    RETURN_NOERROR;
}