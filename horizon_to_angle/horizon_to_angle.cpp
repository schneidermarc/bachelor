//
// Created by marc on 19.07.19.
//

#include "horizon_to_angle.h"
#include <sstream>
#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <cmath>
#include<math.h>

using namespace std;

ADTF_FILTER_PLUGIN("horizon_to_angle", OID_ADTF_horizon_to_angle, horizon_to_angle);

horizon_to_angle::horizon_to_angle(const tChar *__info) {
    enabled = false;
    oldAngleTaken = false;
}

horizon_to_angle::~horizon_to_angle() {

}

tResult horizon_to_angle::Init(cFilter::tInitStage eStage, IException **__exception_ptr) {
    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult horizon_to_angle::registerInputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaType> pInputType;
    RETURN_IF_FAILED(AllocMediaType(&pInputType, MEDIA_TYPE_STRUCTURED_DATA, MEDIA_SUBTYPE_STRUCT_STRUCTURED, __exception_ptr));

    RETURN_IF_FAILED(lidar_in.Create("input_rplidar", pInputType, this));
    RETURN_IF_FAILED(RegisterPin(&lidar_in));

    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(angleInput.Create("angleInput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&angleInput));

    RETURN_IF_FAILED(enabler.Create("Enabler", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&enabler));

    RETURN_NOERROR;
}

tResult horizon_to_angle::registerOutputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(angleOutput.Create("angleOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&angleOutput));

    RETURN_IF_FAILED(finished.Create("finished", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&finished));

    RETURN_NOERROR;
}
tResult horizon_to_angle::Shutdown(cFilter::tInitStage eStage, IException **__exception_ptr) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);

    RETURN_NOERROR;
}


tResult horizon_to_angle::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                                 IMediaSample *pMediaSample) {
    if (nEventCode == IPinEventSink::PE_MediaSampleReceived) {
        RETURN_IF_POINTER_NULL(pMediaSample);

        tSignalValue angleData;
        tBoolSignalValue enablerData;

        if(pSource == &angleInput){
            __sample_read_lock(pMediaSample, tSignalValue, pData);
            angleData = *pData;

        }
        if (pSource == &enabler) {
        __sample_read_lock(pMediaSample, tBoolSignalValue, pData);
        enablerData = *pData;

        }

        if(enablerData.bValue == tTrue) {
            enabled = true;

        }
        if(!oldAngleTaken && enabled){
            std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            oldAngle = abs(angleData.f32Value);
            oldAngleTaken = true;
        }

        if(pSource == &lidar_in) {
                cPointExt<tFloat64> *l_pSrcBuffer;
                std::vector<cPointExt<tFloat64>> m_vpoints;
                std::vector<cPointExt<tFloat64>> temp;
                if (IS_OK(pMediaSample->Lock((const tVoid **) &l_pSrcBuffer))) {
                    m_vpoints.clear();
                    m_vpoints.assign(l_pSrcBuffer,
                                     l_pSrcBuffer + pMediaSample->GetSize() / sizeof(cPointExt<tFloat64>));
                    std::copy_if(m_vpoints.begin(), m_vpoints.end(), std::back_inserter(temp),
                                 [](cPointExt<tFloat64> i) {
                                     return i.GetXValue() != 0 && i.GetYValue() != 0 && i.GetXValue() >= 0 &&
                                            i.GetXValue() < 2000 &&
                                            i.GetYValue() >= -500 && i.GetYValue() <= 500;
                                 });
                    m_vpoints = temp;
                    std::stringstream ss;
                    ss << "{\"points\":[";
/*
                    for (unsigned i = 0; i < m_vpoints.size(); ++i) {
                        ss << "[" << m_vpoints[i].GetX() << "," << m_vpoints[i].GetY() << "],";
                    }
*/
                    //CALCULATION: LINE OF BEST FIT / LINEAR REGRESSION OVER THE SET OF LiDAR DATA

                    tFloat32 sumX{0.0};
                    tFloat32 sumY{0.0};
                    tFloat32 avgX{0.0};
                    tFloat32 avgY{0.0};
                    tFloat32 numerator{0.0};
                    tFloat32 denominator{0.0};

                    for (unsigned i = 0; i < m_vpoints.size(); ++i) {
                        sumX += m_vpoints[i].GetXValue();
                        sumY += m_vpoints[i].GetYValue();
                    }

                    avgX = sumX / m_vpoints.size();
                    avgY = sumY / m_vpoints.size();

                    for (unsigned i = 0; i < m_vpoints.size(); ++i) {
                        numerator += (m_vpoints[i].GetXValue() - avgX) * (m_vpoints[i].GetYValue() - avgY);
                        denominator += (m_vpoints[i].GetXValue() - avgX) * (m_vpoints[i].GetXValue() - avgX);
                    }

                    if (denominator == 0) {
                        RETURN_ERROR(1);
                    }

                    tFloat32 slope = numerator / denominator;

                    //tFloat32 b = avgY - slope * avgX;
/*
                    tFloat32 slopeHorizon = angleData.f32Value;

                    tFloat32 numeratorForAngle = slopeHorizon - slope;
                    tFloat32 denominatorForAngle = 1 + slopeHorizon * slope;

                    tFloat32 angle = abs((atan(abs(numeratorForAngle / denominatorForAngle)) * 180 / M_PI));
*/
                    tFloat32 angle = abs(atan(slope)*180/M_PI);
                    pMediaSample->Unlock((const tVoid **) &l_pSrcBuffer);

                    if(enabled){
                        tUInt32 timestamp{0};
                        tFloat32 actualAngle=abs(calculateDifference(oldAngle, angle));
                        tSignalValue angleValue{timestamp, actualAngle};
                        tBoolSignalValue finishedValue{timestamp, tTrue};

                        std::stringstream slopeStream;
                        slopeStream << "OLDANGLE: " << oldAngle << "NEWANGLE: " << angle <<"ACTUALANGLE: " << actualAngle;
                        std::string slopeString = slopeStream.str();
                        const char *result = slopeString.c_str();

                      //  std::string str = ss.str();
                      //  str.resize(str.size() - 1);
                      //  str += "]}";

                      //  std::string str2 = str;
                      //  const char *pr = str2.c_str();
                      //  LOG_ERROR(pr);
                        LOG_ERROR(result);

                        if(angleValue.f32Value != 0.0) {
                            transmitData(angleValue, finishedValue);
                        }
                    }
                }
        }
    }
    RETURN_NOERROR;
}

tFloat32 horizon_to_angle::calculateDifference(tFloat32 horizonAngle, tFloat32 actualAngle) {
    tFloat32 angleDifference{actualAngle-horizonAngle};
    return angleDifference;
}

tResult horizon_to_angle::transmitData(tSignalValue angleValue, tBoolSignalValue finishedValue){
    cObjectPtr<IMediaSample> angleSample;
    cObjectPtr<IMediaSample> finishedSample;

  //  if (IS_OK(AllocMediaSample(&angleSample))) {

        RETURN_IF_FAILED(AllocMediaSample((tVoid **) &angleSample));
        RETURN_IF_FAILED(
                angleSample->Update(_clock->GetStreamTime(), &angleValue, sizeof(angleValue),
                                    IMediaSample::MSF_None));

        RETURN_IF_FAILED(angleOutput.Transmit(angleSample));
   // }
   // if (IS_OK(AllocMediaSample(&finishedSample))) {

        RETURN_IF_FAILED(AllocMediaSample((tVoid **) &finishedSample));
        RETURN_IF_FAILED(
                finishedSample->Update(_clock->GetStreamTime(), &finishedValue, sizeof(finishedValue),
                                    IMediaSample::MSF_None));

        RETURN_IF_FAILED(finished.Transmit(finishedSample));
    //}

    RETURN_NOERROR;
}