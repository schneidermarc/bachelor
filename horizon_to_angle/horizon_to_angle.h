//
// Created by marc on 19.07.19.
//

#ifndef AADC_USER_HORIZON_TO_ANGLE_H
#define AADC_USER_HORIZON_TO_ANGLE_H

#define OID_ADTF_horizon_to_angle "adtf.example.horizon_to_angle"

#include "stdafx.h"
#include "aadc_structs.h"
#include <chrono>
#include <thread>

class horizon_to_angle : public adtf::cFilter {
    ADTF_FILTER(OID_ADTF_horizon_to_angle, "horizon_to_angle", adtf::OBJCAT_DataFilter);

protected:
    cInputPin angleInput;
    cInputPin lidar_in;
    cInputPin enabler;

    cOutputPin angleOutput;
    cOutputPin finished;

public:
    horizon_to_angle(const tChar *__info);

    virtual ~horizon_to_angle();

protected:
    tResult Init(tInitStage eStage, __exception);

    tResult registerInputPins(__exception);

    tResult registerOutputPins(__exception);

    tResult Shutdown(tInitStage eStage, __exception);

    // implements IPinEventSink
    tResult OnPinEvent(IPin *pSource,
                       tInt nEventCode,
                       tInt nParam1,
                       tInt nParam2,
                       IMediaSample *pMediaSample);

    tFloat32 calculateDifference(tFloat32 horizonAngle, tFloat32 actualAngle);
    tResult transmitData(tSignalValue angleValue, tBoolSignalValue finishedValue);

private:
    cObjectPtr<IMediaTypeDescription> m_pCoderDescSignal;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutputB;

    bool enabled;
    tFloat32 oldAngle;
    bool oldAngleTaken;
};


#endif //AADC_USER_HORIZON_TO_ANGLE_H
