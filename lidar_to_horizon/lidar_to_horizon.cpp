//
// Created by marc on 18.07.19.
//

#include "stdafx.h"
#include "lidar_to_horizon.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <cstdlib>
#include <sstream>
#include "aadc_structs.h"
#include "adtf_graphics.h"
#include "cmath"
#include <chrono>
#include <thread>
using namespace adtf_graphics;
using namespace std;

ADTF_FILTER_PLUGIN("lidar_to_horizon", OID_ADTF_lidar_to_horizon, lidar_to_horizon);

lidar_to_horizon::lidar_to_horizon(const tChar *__info) {
    enabled = false;
}

lidar_to_horizon::~lidar_to_horizon() {

}

tResult lidar_to_horizon::Init(cFilter::tInitStage eStage, IException **__exception_ptr) {
    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult lidar_to_horizon::registerInputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaType> pInputType;
    RETURN_IF_FAILED(AllocMediaType(&pInputType, MEDIA_TYPE_STRUCTURED_DATA, MEDIA_SUBTYPE_STRUCT_STRUCTURED, __exception_ptr))

    RETURN_IF_FAILED(lidar_in.Create("input_rplidar", pInputType, this))
    RETURN_IF_FAILED(RegisterPin(&lidar_in))

    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal))

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(enabler.Create("Enabler", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&enabler));

    RETURN_NOERROR;
}

tResult lidar_to_horizon::registerOutputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal))

    RETURN_IF_FAILED(angleOutput.Create("angleOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)))
    RETURN_IF_FAILED(RegisterPin(&angleOutput))

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(finished.Create("finished", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&finished));

    RETURN_NOERROR;
}

tResult lidar_to_horizon::Shutdown(cFilter::tInitStage eStage, IException **__exception_ptr) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);

    RETURN_NOERROR;
}

tResult lidar_to_horizon::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                                 IMediaSample *pMediaSample) {

    if (nEventCode == IPinEventSink::PE_MediaSampleReceived) {
        RETURN_IF_POINTER_NULL(pMediaSample);


        tBoolSignalValue enablerData;
        // Differ between PINs

        if (pSource == &enabler) {
            __sample_read_lock(pMediaSample, tBoolSignalValue, pData);
            enablerData = *pData;
            if(enablerData.bValue == tTrue) {
                enabled = true;
                LOG_ERROR("TRUE");
            }

        }
        if (pSource == &lidar_in) {
                cPointExt<tFloat64> *l_pSrcBuffer;
                vector<cPointExt<tFloat64>> m_vpoints;
                vector<cPointExt<tFloat64>> temp;
                if (IS_OK(pMediaSample->Lock((const tVoid **) &l_pSrcBuffer))) {
                    m_vpoints.clear();
                    m_vpoints.assign(l_pSrcBuffer,
                                     l_pSrcBuffer + pMediaSample->GetSize() / sizeof(cPointExt<tFloat64>));
                    copy_if(m_vpoints.begin(), m_vpoints.end(), std::back_inserter(temp),
                                 [](cPointExt<tFloat64> i) {
                                     return i.GetXValue() != 0 && i.GetYValue() != 0 && i.GetXValue() >= 0 &&
                                            i.GetXValue() < 2000 && i.GetYValue() >= -500 && i.GetYValue() <= 500;
                                 });
                    m_vpoints = temp;
 /*                   std::stringstream ss;
                    ss << "{\"points\":[";

                    for (unsigned i = 0; i < m_vpoints.size(); ++i) {
                        ss << "[" << m_vpoints[i].GetX() << "," << m_vpoints[i].GetY() << "],";
                    }
*/
                    //CALCULATION: LINE OF BEST FIT / LINEAR REGRESSION OVER THE SET OF LiDAR DATA

                    tFloat32 sumX{0.0};
                    tFloat32 sumY{0.0};
                    tFloat32 avgX{0.0};
                    tFloat32 avgY{0.0};
                    tFloat32 numerator{0.0};
                    tFloat32 denominator{0.0};
                    auto size = (tFloat32)m_vpoints.size();
                    //tFloat32 size = (tFloat32)m_vpoints.size();

                    //calculate sums of X's and Y's
                    for (unsigned i = 0; i < size; ++i) {
                        sumX += m_vpoints[i].GetXValue();
                        sumY += m_vpoints[i].GetYValue();
                    }

                    //calculate the average X's and Y's
                    avgX = (float)((1/size) * sumX);
                    avgY = (float)((1/size) * sumY);

                    //numerator and denominator to calculate the slope
                    for (unsigned i = 0; i < size; ++i) {
                        numerator += (m_vpoints[i].GetXValue() - avgX) * (m_vpoints[i].GetYValue() - avgY);
                        denominator += (m_vpoints[i].GetXValue() - avgX) * (m_vpoints[i].GetXValue() - avgX);
                    }

                    //no division with 0
                    if (denominator == 0) {
                        RETURN_ERROR(1);
                    }

                    //calculate slope(m) and b (y=m*x+b)
                    tFloat32 slope = numerator / denominator;
                    //tFloat32 b = avgY - slope * avgX;
                    //so the linaer regression line is y=slope*x+b

                    tFloat32 alpha = atan(slope)*180/M_PI;
                    tFloat32 beta = 180 - 90 - alpha;

/*                    stringstream slopeStream;
                    slopeStream << "X-angle: " << alpha << ";;;Y-angle: " << beta;
                    string slopeString = slopeStream.str();
                    const char *result = slopeString.c_str();

                    string str = ss.str();
                    str.resize(str.size() - 1);
                    str += "]}";

                    string str2 = str;
                    const char *pr = str2.c_str();
                    LOG_ERROR(pr);
                    LOG_ERROR(result);
*/

                    pMediaSample->Unlock((const tVoid **) &l_pSrcBuffer);
                    if(enabled) {


                        tUInt32 timestamp{0};
                        tSignalValue angleValue{timestamp, alpha};
                        tBoolSignalValue finishedValue{timestamp, tTrue};

                        cObjectPtr<IMediaSample> angleSample;
                        cObjectPtr<IMediaSample> finishedSample;

                  //      if (IS_OK(AllocMediaSample(&angleSample))) {

                            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &angleSample))
                            RETURN_IF_FAILED(
                                    angleSample->Update(_clock->GetStreamTime(), &angleValue, sizeof(angleValue),
                                                        IMediaSample::MSF_None))

                            RETURN_IF_FAILED(angleOutput.Transmit(angleSample))
                 //       }

                 //       if(IS_OK(AllocMediaSample(&finishedSample))){
                            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &finishedSample))
                            RETURN_IF_FAILED(
                                    finishedSample->Update(_clock->GetStreamTime(), &finishedValue, sizeof(finishedValue),
                                                        IMediaSample::MSF_None))

                            RETURN_IF_FAILED(finished.Transmit(finishedSample))
                 //       }
                    }
                }
        }
    }
    RETURN_NOERROR;
}

