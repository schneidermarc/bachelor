//
// Created by marc on 18.07.19.
//

#ifndef AADC_USER_LIDAR_TO_HORIZON_H
#define AADC_USER_LIDAR_TO_HORIZON_H

#define OID_ADTF_lidar_to_horizon "adtf.example.lidar_to_horizon"

#include <map>
#include <vector>
#include "stdafx.h"
#include "aadc_structs.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <limits>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <math.h>
#include <cstdlib>
#include <sstream>
#include "adtf_graphics.h"
#include "boost/circular_buffer.hpp"

using namespace adtf_graphics;
using namespace std;

class lidar_to_horizon : public adtf::cFilter {
ADTF_FILTER(OID_ADTF_lidar_to_horizon, "lidar_to_horizon", adtf::OBJCAT_DataFilter);

protected:
    cInputPin lidar_in;
    cInputPin enabler;
    cOutputPin angleOutput;
    cOutputPin finished;

public:
    lidar_to_horizon(const tChar *__info);

    virtual ~lidar_to_horizon();

protected:
    tResult Init(tInitStage eStage, __exception);

    tResult registerInputPins(__exception);

    tResult registerOutputPins(__exception);

    tResult Shutdown(tInitStage eStage, __exception);

    // implements IPinEventSink
    tResult OnPinEvent(IPin *pSource,
                       tInt nEventCode,
                       tInt nParam1,
                       tInt nParam2,
                       IMediaSample *pMediaSample);

private:
    cObjectPtr<IMediaTypeDescription> m_pDescriptionInerMeasUnitData;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutputB;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescSignal;
    bool enabled;
};
#endif //AADC_USER_LIDAR_TO_HORIZON_H
