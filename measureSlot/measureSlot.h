//
// Created by maschn on 09.05.19.
//

#ifndef AADC_USER_MEASURESLOT_H
#define AADC_USER_MEASURESLOT_H


#include "stdafx.h"
#include "aadc_structs.h"
#include "../adtf_extensions/MediaTypeInputPin.h"
#include <ctime>
#include <iostream>
#include <chrono>

using namespace std::chrono;;

#define OID_ADTF_MeasureSlot "adtf.example.measureSlot"
#define DISPLAY_FILTER_NAME "measureSlot"

class measureSlot : public adtf::cFilter
{
    ADTF_FILTER(OID_ADTF_MeasureSlot, "measureSlot", adtf::OBJCAT_DataFilter);

public:
    // input PIN:
    MediaTypeInputPin ussStructInputPin;
    cInputPin InputWheelLeft;
    cInputPin InputWheelRight;
    cInputPin enabler;

    // output PIN:
    cOutputPin speedOutput;
    cOutputPin steeringOutput;
    cOutputPin finished;


    measureSlot(const tChar* __info);
    virtual ~measureSlot();


protected:
    // Custom PIN registration methods:
    tResult registerInputPins(__exception);
    tResult registerOutputPins(__exception);

    tResult Init(tInitStage eStage, ucom::IException** __exception_ptr);
    tResult Shutdown(tInitStage eStage, ucom::IException** __exception_ptr = NULL);
    tResult OnPinEvent(IPin* pSource, tInt nEventCode, tInt nParam1, tInt nParam2, IMediaSample* pMediaSample);
    tResult calculateSize(tUltrasonicStruct ultrasonicStruct, tBoolSignalValue enabler, tUInt32 leftWheelTach, tUInt32 rightWheelTach);
    tUInt32 ProcessWheelSampleLeft(IMediaSample* pMediaSample);
    tUInt32 ProcessWheelSampleRight(IMediaSample* pMediaSample);

private:
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutput;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescSignal;
    cObjectPtr<IMediaTypeDescription> m_pCoderDescOutputB;
    cObjectPtr<IMediaTypeDescription> m_pDescriptionWheelRightData;
    cObjectPtr<IMediaTypeDescription> m_pDescriptionWheelLeftData;
    cCriticalSection m_oCritSectionReceive;

    bool enabled;

    bool phaseZero;
    bool phaseOne;
    bool phaseTwo;
    bool phaseThree;

    tUInt32 rightWheelTicks;
    tUInt32 leftWheelTicks;
    tUInt32 counter;
    tUInt32 wheelTicks;
    tUInt32 ticksNow;
    tFloat32 distanceRight;

};
#endif //AADC_USER_MEASURESLOT_H
