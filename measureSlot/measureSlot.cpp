//
// Created by maschn on 09.05.19.
//
#include "measureSlot.h"
#include <ctime>
#include <iostream>
#include <sstream>
#include <aadc_structs.h>

ADTF_FILTER_PLUGIN(DISPLAY_FILTER_NAME, OID_ADTF_MeasureSlot, measureSlot);

measureSlot::measureSlot(const tChar *__info)  : cFilter(__info){
    enabled = false;
    phaseZero = true;
    phaseOne = true;
    phaseThree = false;
    counter = 0;
}

measureSlot::~measureSlot() {

}

tResult measureSlot::Init(cFilter::tInitStage eStage, ucom::IException **__exception_ptr) {
    THROW_IF_FAILED(cFilter::Init(eStage, __exception_ptr))

    switch (eStage) {
        case StageFirst: {
            THROW_IF_FAILED(registerInputPins(__exception_ptr));
            THROW_IF_FAILED(registerOutputPins(__exception_ptr));
            break;
        }
        case StageNormal: {
            break;
        }
        default: {
            break;
        }
    }
    RETURN_NOERROR;
}

tResult measureSlot::registerInputPins(IException **__exception_ptr) {
    THROW_IF_FAILED(ussStructInputPin.Create("USSDataIn", "tUltrasonicStruct", static_cast<IPinEventSink *>(this),
                                             __exception_ptr))
    THROW_IF_FAILED(RegisterPin(&ussStructInputPin))


    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER,
                                         (tVoid **) &pDescManager, __exception_ptr))

    tChar const * strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0,0,0, "tSignalValue", strDescSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeSignalValue->GetInterface(IID_ADTF_MEDIA_DESCRIPTION, (tVoid**)&m_pCoderDescSignal))

    tChar const * strDescWheelData = pDescManager->GetMediaDescription("tWheelData");
    RETURN_IF_POINTER_NULL(strDescWheelData)
    cObjectPtr<IMediaType> pTypeWheelData = new cMediaType(0, 0, 0, "tWheelData", strDescWheelData, IMediaDescription::MDF_DDL_DEFAULT_VERSION);

    RETURN_IF_FAILED(pTypeWheelData->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pDescriptionWheelLeftData))
    RETURN_IF_FAILED(pTypeWheelData->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pDescriptionWheelRightData))

    RETURN_IF_FAILED(InputWheelLeft.Create("WheelLeftStruct", pTypeWheelData, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&InputWheelLeft));

    RETURN_IF_FAILED(InputWheelRight.Create("WheelRightStruct", pTypeWheelData, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&InputWheelRight));

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue)
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB))

    RETURN_IF_FAILED(enabler.Create("Enabler", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)))
    RETURN_IF_FAILED(RegisterPin(&enabler))

    RETURN_NOERROR;
}

tResult measureSlot::registerOutputPins(IException **__exception_ptr) {
    cObjectPtr<IMediaDescriptionManager> pDescManager;
    RETURN_IF_FAILED(_runtime->GetObject(OID_ADTF_MEDIA_DESCRIPTION_MANAGER, IID_ADTF_MEDIA_DESCRIPTION_MANAGER, (tVoid**)&pDescManager, __exception_ptr));

    tChar const *strDescSignalValue = pDescManager->GetMediaDescription("tSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeSignalValue = new cMediaType(0, 0, 0, "tSignalValue", strDescSignalValue,
                                                             IMediaDescription::MDF_DDL_DEFAULT_VERSION);

    tChar const * strDescBoolSignalValue = pDescManager->GetMediaDescription("tBoolSignalValue");
    RETURN_IF_POINTER_NULL(strDescSignalValue);
    cObjectPtr<IMediaType> pTypeBoolSignalValue = new cMediaType(0, 0, 0, "tBoolSignalValue", strDescBoolSignalValue, IMediaDescription::MDF_DDL_DEFAULT_VERSION);
    RETURN_IF_FAILED(pTypeBoolSignalValue->GetInterface(IID_ADTF_MEDIA_TYPE_DESCRIPTION, (tVoid**)&m_pCoderDescOutputB));

    RETURN_IF_FAILED(finished.Create("finished", pTypeBoolSignalValue, static_cast<IPinEventSink*> (this)));
    RETURN_IF_FAILED(RegisterPin(&finished));

    RETURN_IF_FAILED(speedOutput.Create("SpeedOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&speedOutput));

    RETURN_IF_FAILED(steeringOutput.Create("SteeringOutput", pTypeSignalValue, static_cast<IPinEventSink *>(this)));
    RETURN_IF_FAILED(RegisterPin(&steeringOutput));

    RETURN_NOERROR;
}

tResult measureSlot::Shutdown(cFilter::tInitStage eStage, IException **__exception_ptr) {
    if (eStage == StageGraphReady) {
    } else if (eStage == StageNormal) {
    } else if (eStage == StageFirst) {
    }

    return cFilter::Shutdown(eStage, __exception_ptr);
}

tResult measureSlot::OnPinEvent(IPin *pSource, tInt nEventCode, tInt nParam1, tInt nParam2,
                              IMediaSample *pMediaSample) {

    __synchronized_obj(m_oCritSectionReceive);

    if (nEventCode == IPinEventSink::PE_MediaSampleReceived
        && pMediaSample != NULL) {

        tUltrasonicStruct ultrasonicData;
        tBoolSignalValue enablerData;

        if (pSource == &ussStructInputPin) {
            __sample_read_lock(pMediaSample, tUltrasonicStruct, pData);
            ultrasonicData = *pData;
        } else if(pSource == & InputWheelLeft) {

            RETURN_IF_FAILED(leftWheelTicks = ProcessWheelSampleLeft(pMediaSample));
        } else if(pSource == &InputWheelRight){

            RETURN_IF_FAILED(rightWheelTicks = ProcessWheelSampleRight(pMediaSample));
        } else if (pSource == &enabler) {
            __sample_read_lock(pMediaSample, tBoolSignalValue, pData);
            enablerData = *pData;

            if(enablerData.bValue == tTrue) {
                enabled = true;
            }
        }

        if(enabled){
            calculateSize(ultrasonicData, enablerData, leftWheelTicks, rightWheelTicks);
        } else {
            LOG_ERROR("DON'T DRIVE");
            tSignalValue speedValue{0,0};
            cObjectPtr<IMediaSample> pSpeed;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pSpeed));
            RETURN_IF_FAILED(pSpeed->Update(_clock->GetStreamTime(), &speedValue, sizeof(speedValue),
                                            IMediaSample::MSF_None));

            RETURN_IF_FAILED(speedOutput.Transmit(pSpeed));

            tSignalValue steeringValue{0, 0};

            cObjectPtr<IMediaSample> pSteering;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pSteering));
            RETURN_IF_FAILED(pSteering->Update(_clock->GetStreamTime(), &steeringValue, sizeof(steeringValue),
                                               IMediaSample::MSF_None));

            RETURN_IF_FAILED(steeringOutput.Transmit(pSteering));
        }
    }
    RETURN_NOERROR;
}

tResult measureSlot::calculateSize(tUltrasonicStruct ultrasonicStruct, tBoolSignalValue enabler, tUInt32 leftWheelTach, tUInt32 rightWheelTach) {

    tBool finishedBool{tFalse};
    tUInt32 timestamp{0};
    tFloat32 speed{0};
    tFloat32 steering{0};
    counter = ticksNow - wheelTicks;

    if(ultrasonicStruct.tSideRight.f32Value > 0) {
        distanceRight = ultrasonicStruct.tSideRight.f32Value;
    }
    std::stringstream ss;
    ss << "COUNTER: " << counter << ";; DISTANZ: " << distanceRight;
    std::string angleString = ss.str();
    const char *result = angleString.c_str();
    LOG_ERROR(result);

    if (distanceRight < 45) {
        LOG_ERROR("KEINE LÜCKE");
        wheelTicks = (leftWheelTach + rightWheelTach) / 2;
        ticksNow =  (leftWheelTach + rightWheelTach) / 2;
        speed = -10;
        steering = 0;
        finishedBool = tFalse;
        phaseZero= false;
        phaseTwo = true;

    } else if ((distanceRight >= 45 && (counter < 250) && phaseTwo)) {
        LOG_ERROR("MESSE LÜCKE - LÜCKE (NOCH) ZU KLEIN");
        ticksNow =  (leftWheelTach + rightWheelTach) / 2;
        speed = -10;
        steering = 0;
        finishedBool = tFalse;
        //   phaseOne = false;
        phaseThree = true;
    } else if((distanceRight >=45) && (phaseZero || phaseThree)){
        ticksNow =  (leftWheelTach + rightWheelTach) / 2;
        speed = 0;
        steering = 0;
        if(phaseZero) {
            LOG_ERROR("Fehler");
            phaseOne = true;
            finishedBool = tFalse;
        } else if (phaseThree){
            LOG_ERROR("LÜCKE GEFUNDEN");
            phaseZero = false;
            phaseOne = false;
            phaseTwo = false;
            phaseThree = false;
            finishedBool = tTrue;

            tBoolSignalValue finishedValue{timestamp, finishedBool};
            cObjectPtr<IMediaSample> pFinished;
            RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pFinished));
            RETURN_IF_FAILED(pFinished->Update(_clock->GetStreamTime(), &finishedValue, sizeof(finishedValue),
                                               IMediaSample::MSF_None));

            RETURN_IF_FAILED(finished.Transmit(pFinished));
        }
    }


    tSignalValue speedValue{timestamp,speed};
    cObjectPtr<IMediaSample> pSpeed;
    RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pSpeed));
    RETURN_IF_FAILED(pSpeed->Update(_clock->GetStreamTime(), &speedValue, sizeof(speedValue),
            IMediaSample::MSF_None));

    RETURN_IF_FAILED(speedOutput.Transmit(pSpeed));

    tSignalValue steeringValue{timestamp, steering};

    cObjectPtr<IMediaSample> pSteering;
    RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pSteering));
    RETURN_IF_FAILED(pSteering->Update(_clock->GetStreamTime(), &steeringValue, sizeof(steeringValue),
            IMediaSample::MSF_None));

    RETURN_IF_FAILED(steeringOutput.Transmit(pSteering));

         /*
                 tBoolSignalValue finishedValue{timestamp, finishedBool};
                 cObjectPtr<IMediaSample> pFinished;
                 RETURN_IF_FAILED(AllocMediaSample((tVoid **) &pFinished));
                 RETURN_IF_FAILED(pFinished->Update(_clock->GetStreamTime(), &finishedValue, sizeof(finishedValue),
                                                    IMediaSample::MSF_None));

                 RETURN_IF_FAILED(finished.Transmit(pFinished));*/



    RETURN_NOERROR;
}

tUInt32 measureSlot::ProcessWheelSampleLeft(IMediaSample* pMediaSample)
{
    tUInt32 ui32Tach = 0;
    tInt8 i8Direction = 0;

    static bool hasID = false;
    static tBufferID szIDWheelDataI8WheelDir;
    static tBufferID szIDWheelDataUi32WheelTach;
    static tBufferID szIDWheelDataArduinoTimestamp;

    {
        __adtf_sample_read_lock_mediadescription(m_pDescriptionWheelLeftData, pMediaSample, pCoderInput);

        if (!hasID)
        {
            pCoderInput->GetID("i8WheelDir", szIDWheelDataI8WheelDir);
            pCoderInput->GetID("ui32WheelTach", szIDWheelDataUi32WheelTach);
            pCoderInput->GetID("ui32ArduinoTimestamp", szIDWheelDataArduinoTimestamp);
            hasID = tTrue;
        }

        pCoderInput->Get(szIDWheelDataUi32WheelTach, (tVoid*)&ui32Tach);
        pCoderInput->Get(szIDWheelDataI8WheelDir, (tVoid*)&i8Direction);

    }
    return ui32Tach;
}



tUInt32 measureSlot::ProcessWheelSampleRight(IMediaSample* pMediaSample)
{
    tUInt32 ui32Tach = 0;
    tInt8 i8Direction = 0;

    static bool hasID = false;
    static tBufferID szIDWheelDataI8WheelDir;
    static tBufferID szIDWheelDataUi32WheelTach;
    static tBufferID szIDWheelDataArduinoTimestamp;

    {
        __adtf_sample_read_lock_mediadescription(m_pDescriptionWheelRightData, pMediaSample, pCoderInput);

        if (!hasID)
        {
            pCoderInput->GetID("i8WheelDir", szIDWheelDataI8WheelDir);
            pCoderInput->GetID("ui32WheelTach", szIDWheelDataUi32WheelTach);
            pCoderInput->GetID("ui32ArduinoTimestamp", szIDWheelDataArduinoTimestamp);
            hasID = tTrue;
        }

        pCoderInput->Get(szIDWheelDataUi32WheelTach, (tVoid*)&ui32Tach);
        pCoderInput->Get(szIDWheelDataI8WheelDir, (tVoid*)&i8Direction);

    }
    return ui32Tach;
}
